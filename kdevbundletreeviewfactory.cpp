/*
    SPDX-FileCopyrightText: 2012-2012 Sandro Andrade <sandroandrade@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "kdevbundletreeviewfactory.h"
#include "kdevbundledtreeviewplugin.h"
#include "bundledtreeview.h"

#include <QWidget>

K_PLUGIN_FACTORY_WITH_JSON(BundledTreeViewFactory, "kdevbundledtreeviewplugin.json", registerPlugin<KDevBundledTreeViewPlugin>(); )

KDevBundledTreeViewFactory::KDevBundledTreeViewFactory(KDevBundledTreeViewPlugin *plugin)
    : m_plugin(plugin)
{}

QWidget* KDevBundledTreeViewFactory::create(QWidget *parent)
{
    return new BundledTreeView(m_plugin, parent);
}

Qt::DockWidgetArea KDevBundledTreeViewFactory::defaultPosition() const
{
    return Qt::RightDockWidgetArea;
}

QString KDevBundledTreeViewFactory::id() const
{
    return "org.kdevelop.BundledTreeView";
}

QList<QAction*> KDevBundledTreeViewFactory::toolBarActions(QWidget* viewWidget) const
{
    Q_UNUSED(viewWidget);
    return {};
}

QList<QAction*> KDevBundledTreeViewFactory::contextMenuActions(QWidget* viewWidget) const
{
    Q_UNUSED(viewWidget);
    return {};
}

void KDevBundledTreeViewFactory::viewCreated(Sublime::View* view)
{
    Q_UNUSED(view);
}

bool KDevBundledTreeViewFactory::allowMultiple() const
{
    return false;
}

#include "kdevbundletreeviewfactory.moc"
