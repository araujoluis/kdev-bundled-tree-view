/*
    SPDX-FileCopyrightText: 2012-2012 Sandro Andrade <sandroandrade@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef KDEVBUNDLETREEVIEWFACTORY_H
#define KDEVBUNDLETREEVIEWFACTORY_H

#include <KPluginFactory>
#include <interfaces/iuicontroller.h>

class QWidget;
class KDevBundledTreeViewPlugin;

class KDevBundledTreeViewFactory : public KDevelop::IToolViewFactory
{
public:
    KDevBundledTreeViewFactory(KDevBundledTreeViewPlugin *plugin);
    ~KDevBundledTreeViewFactory() = default;

    QWidget* create(QWidget *parent = nullptr) override;
    Qt::DockWidgetArea defaultPosition() const override;
    QString id() const override;
    QList<QAction*> toolBarActions(QWidget* viewWidget) const override;
    QList<QAction*> contextMenuActions(QWidget* viewWidget) const override;
    void viewCreated(Sublime::View* view) override;
    bool allowMultiple() const override;

private:
    KDevBundledTreeViewPlugin *m_plugin;
};

#endif  // KDEVBUNDLETREEVIEWFACTORY_H
